<?php
/**
 * Created by PhpStorm.
 * User: Roman Baranov
 * Date: 02.03.16
 * Time: 10:44
 */

error_reporting(0);
require_once('app/CurlPipeDrive.php');
require_once('app/DealModel.php');
require_once('app/Logger.php');

$pipe = new CurlPipeDrive();
$mod = new DealModel();

$deals = $mod->getAll();
$count_upd = 0;
if(count($deals)){
    foreach ($deals as $item) {
        $mod->table = "deals_change";
        $updates = $pipe->getUpdateDeals($item['id']);
        if($updates->success){
            $last_upd = null;
            foreach($updates->data as $upd_item){
                if($upd_item->object == "dealChange"){
                    $mod->add($upd_item->data);
                    $last_upd = $upd_item->data;
                }
            }

            if($last_upd != null){
                $mod->table = "deals";
                $mod->update($last_upd->item_id, $last_upd->field_key, $last_upd->new_value);
            }

            echo "\ndeal id - " . $item['id'] . ", ";
            $mod->table = "deals_change";

            if(!count($mod->fields)){
                continue;
            }

            if($mod->insert()->execute()){
                $count = count($updates->data);
                echo $count . " deal updates successful pushed or replaced.";
                $count_upd += $count;
            }else{
                $msg = "push_all_update_deals_cron.php - Error updates push deals.";
                echo $msg;
                Logger::add($msg);
            }
        }
    }
    Logger::add("push_all_update_deals_cron.php - " . $count_upd . " deal updates successful pushed or replaced.");
}else{
    $msg = 'Not have deals';
    echo $msg;
    Logger::add($msg, true);
}
