<?php
/**
 * Created by PhpStorm.
 * User: Roman Baranov
 * Date: 02.03.16
 * Time: 10:44
 */

error_reporting(0);
require_once('app/CurlPipeDrive.php');
require_once('app/DealModel.php');
require_once('app/Logger.php');

$pipe = new CurlPipeDrive();
$mod = new DealModel();

$deals = $pipe->getTimeLineDeals(date('Y-m-d'), 'day', 1);

if(!count($deals)){
    $msg = "push_today_deals_cron.php - Today is not have deals.";
    echo $msg;
    Logger::add($msg);
    die;
}

foreach($deals as $deal){
    $mod->add($deal);
}

if($mod->insert()->execute()){
    $msg = "push_today_deals_cron.php - " . count($deals) . " deals successful pushed or replaced.";
    echo $msg;
    Logger::add($msg);
}else{
    $msg = "push_today_deals_cron.php - Error push today deals.";
    echo $msg;
    Logger::add($msg, true);
}