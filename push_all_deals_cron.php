<?php
/**
 * Created by PhpStorm.
 * User: Roman Baranov
 * Date: 24.02.16
 * Time: 17:53
 */

error_reporting(0);
require_once('app/CurlPipeDrive.php');
require_once('app/DealModel.php');
require_once('app/Logger.php');

$pipe = new CurlPipeDrive();
$mod = new DealModel();

$deals = $pipe->getAllDeals();

foreach($deals as $deal){
    $mod->add($deal);
}
$msg = "";
if($mod->insert()->execute()){
    $msg = "push_all_deals_cron.php - " . count($deals) . " deals successful pushed or replaced.";
    echo $msg;
    Logger::add($msg);
}else{
    $msg = "Error push deals.";
    echo $msg;
    Logger::add($msg, true);
}

