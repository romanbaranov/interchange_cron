<?php

class CurlPipeDrive
{
    public $URL;
    public $token;

    public function __construct()
    {
        $this->URL = 'api.pipedrive.com';
        $this->token = '0234fa22dbbaa6f652d8ab4201b01eb3bd13eeac';
    }

    public function getTimeLineDeals($start_date, $interval, $amount)
    {
        $fields = array(
            'start_date' => $start_date,
            'interval' => $interval,
            'amount' => $amount,
            'field_key' => 'add_time',
            'exclude_deals' => 0,
            'api_token' => $this->token,
        );

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL,'https://' . $this->URL . '/v1/deals/timeline?' . http_build_query($fields));

        curl_setopt($ch,CURLOPT_HTTPHEADER,
            array('Connection: Keep-Alive',
                  'Content-Type: application/json; charset=UTF-8'
            ));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $response = curl_exec($ch);
        curl_close($ch);
        $deals = json_decode($response);
        if($deals->success){
            $deals_arr = [];
            foreach($deals->data as $deals_item){
                foreach($deals_item->deals as $deal){
                    $deals_arr[] = $deal;
                }
            }
            return $deals_arr;
        }else{
            return false;
        }
    }

    public function getDeal($deal_id)
    {
        $fields = array(
            'api_token' => $this->token,
        );

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL,'https://' . $this->URL . '/v1/deals/' . $deal_id . '?' . http_build_query($fields));

        curl_setopt($ch,CURLOPT_HTTPHEADER,
            array('Connection: Keep-Alive',
                'Content-Type: application/json; charset=UTF-8'
            ));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $response = curl_exec($ch);
        curl_close($ch);
        return json_decode($response);
    }

    public function getDeals($limit = 500, $start = 0)
    {
        $fields = array(
            'api_token' => $this->token,
            'start' => $start,
            'limit' => $limit
        );

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL,'https://' . $this->URL . '/v1/deals?' . http_build_query($fields));

        curl_setopt($ch,CURLOPT_HTTPHEADER,
            array('Connection: Keep-Alive',
                'Content-Type: application/json; charset=UTF-8'
            ));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $response = curl_exec($ch);
        curl_close($ch);
        return json_decode($response);
    }

    public function getAllDeals()
    {
        $deals = [];
        $i = 0;
        $deals_arr = [];
        while(true){
            $deals = $this->getDeals(500, $i*500);
            $deals_arr = array_merge($deals_arr, $deals->data);
            var_dump(count($deals_arr));
            if(!$deals->additional_data->pagination->more_items_in_collection || $deals->data == null){
                break;
            }
            $i++;
        }
        return $deals_arr;
//        $fdeal = $this->getDeal(1);
//        if(isset($fdeal->data->add_time)){
//            $start_date = date('Y-m-d', strtotime($fdeal->data->add_time));
//        }else{
//            return false;
//        }
//        $amount = ceil(((time()-strtotime($start_date))-86400)/86400);
//        var_dump($start_date);
//        return $this->getTimeLineDeals($start_date, 'day', $amount);
    }

    public function getUpdateDeals($deal_id)
    {
        $fields = array(
            'start' => 0,
            'limit' => 500,
            'api_token' => $this->token,
        );

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL,'https://' . $this->URL . '/v1/deals/' . $deal_id . '/flow?' . http_build_query($fields));

        curl_setopt($ch,CURLOPT_HTTPHEADER,
            array('Connection: Keep-Alive',
                'Content-Type: application/json; charset=UTF-8'
            ));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $response = curl_exec($ch);
        curl_close($ch);
        return json_decode($response);
    }
}