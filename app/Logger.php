<?php

class Logger
{
    public $log_file;
    public $err_log_file;

    public static function add($str, $err = false)
    {
        $conf = require(dirname(__FILE__) . '/../config.php');
        if(!isset($conf['path_logs'])){
            $conf['path_logs'] = "/";
        }

        if($err){
            $file = trim($conf['path_logs'], '/') . '/' . 'error_log.txt';
            $content = file_get_contents($file);
            file_put_contents($file, $content . "\n" . $str . " " . date('Y-m-d H:i:s'));
        }else{
            $file = trim($conf['path_logs'], '/') . '/' . 'log.txt';
            $content = file_get_contents($file);
            file_put_contents($file, $content . "\n" . $str . " " . date('Y-m-d H:i:s'));
        }
    }

    public static function clear()
    {
        $conf = require(dirname(__FILE__) . '/../config.php');
        if(!isset($conf['path_logs'])){
            $conf['path_logs'] = "/";
        }

        $file = trim($conf['path_logs'], '/') . '/' . 'log.txt';
        file_put_contents($file, "");
        $file = trim($conf['path_logs'], '/') . '/' . 'error_log.txt';
        file_put_contents($file, "");
    }
}