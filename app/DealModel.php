<?php

require_once('DbDriver.php');
require_once('Logger.php');

class DealModel
{
    public $fields;
    private $items;
    public $table;
    public $query;
    private $multi;
    private $new_fields;

    public function __construct()
    {
        $this->table = "deals";
        $this->fields = [];
        $this->items = [];
        $this->query = "";
        $this->multi = false;
        $this->new_fields = [];
    }

    public function get($where)
    {
        $db = new DbDriver();
        $sql = "SELECT * FROM " . $this->table;
        if(strlen($where)){
            $sql .= " WHERE " . $where;
        }
       // return $sql;
        $res = $db->query($sql);
        $deals = [];
        foreach($res as $item){
            $deals[] = $item;
        }
        return $deals;
    }

    public function getAll()
    {
        $db = new DbDriver();
        $res = $db->query("SELECT * FROM " . $this->table);
        $deals = [];
        foreach($res as $item){
            $deals[] = $item;
        }
        return $deals;
    }

    public function setFields($obj)
    {
        foreach($obj as $key => $val){
            if($this->checkField($key)) {
                $this->fields[] = $key;
            }else{
                $this->new_fields[] = $key;
                $msg = "--- PipeDrive API is have new field '{$this->table}.{$key}', please add this in your db!";
                echo $msg;
                Logger::add($msg);
                Logger::add($msg, true);
            }
        }

        return $this;
    }

    private function checkField($field)
    {
        $db = new DbDriver();
        $res = $db->query("SHOW COLUMNS FROM `{$this->table}` LIKE '{$field}'");
        return $res->num_rows;
    }

    public function add($obj)
    {
        if(!count($this->fields)){
            $this->setFields($obj);
        }
        $this->items[] = $obj;
        return $this;
    }

    public function update($id, $field, $value)
    {
        $db = new DbDriver();
        $sql = "UPDATE `{$this->table}` SET `{$field}`='{$value}' WHERE `id`='{$id}'";
        return $db->query($sql);
    }

    public function insert()
    {
        if(!count($this->fields)){
            return false;
        }

        $fields = "REPLACE INTO `{$this->table}` (`" . implode("`, `", $this->fields) . "`) VALUES ";
        $this->query = "";
        $add_items = [];
        $i = 0;

        foreach($this->items as $item){
            $deal = [];
            foreach($item as $key => $field){
                if(in_array($key, $this->new_fields)){
                    continue;
                }
                if(gettype($field) == 'boolean'){
                    $deal[$key] = ($field)?'1':'0';
                }else if(gettype($field) == 'string') {
                    $deal[$key] = addslashes($field);
                }else if(is_array($field) || is_object($field)){
                    /***  This temporary solution (hardcode). :( :( :(  ***/
                    if(isset($field->id)){
                        $deal[$key] = $field->id;
                    }else if(isset($field->value)){
                        $deal[$key] = $field->value;
                    }else{
                        $deal[$key] = json_encode($field);
                    }
                }else{
                    $deal[$key] = $field;
                }
            }

            $deals_order = [];
            foreach($this->fields as $field){
                if(isset($deal[$field])){
                    $deals_order[] = $deal[$field];
                }else{
                    $deals_order[] = "";
                }
            }

            $str_ins = "";
            foreach($deals_order as $key => $dl){
                if($dl == null){
                    $str_ins .= "NULL";
                }else{
                    $str_ins .= " '" . $dl . "'";
                }

                if($key != count($deal)-1){
                    $str_ins .= ",";
                }
            }

            $add_items[] = $str_ins;//"'" . implode("', '", $deal) . "'";

            if($i % 500 == 0 && $i != 0) {
                $this->query .= $fields . "(" . implode("),\n (", $add_items) . ");\n ";
                $add_items = [];
                $this->multi = true;
            }
            $i++;
        }

        if(count($add_items)) {
            $this->query .= $fields . "(" . implode("), (", $add_items) . ");\n";
        }

        return $this;
    }

    public function execute()
    {
        $this->items = [];
        $this->fields = [];
        $db = new DbDriver();
        return $db->query($this->query, $this->multi);
    }
}