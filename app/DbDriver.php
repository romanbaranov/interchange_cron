<?php

class DbDriver
{
    public $con;
    public function __construct()
    {
        $conf = require(dirname(__FILE__) . '/../config.php');
        $this->con = mysqli_connect($conf['host'], $conf['user'], $conf['password'], $conf['db']) or die('\nCould not connect');
        mysqli_query($this->con, 'SET NAMES utf8');
        return $this->con;
    }

    public function query($query, $multi = false){
        if($multi) {
            $res = mysqli_multi_query($this->con, $query) or die ('\nUnable to execute query. '. mysqli_error($this->con));
        }else{
            $res = mysqli_query($this->con, $query) or die ('\nUnable to execute query. '. mysqli_error($this->con));
        }
        return $res;
    }

    public function __destruct()
    {
        mysqli_close($this->con);
    }
}